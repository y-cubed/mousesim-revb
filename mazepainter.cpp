#include "mazepainter.h"

using namespace MazeSolver;

const int MazePainter::graphics_width=320;

MazePainter::MazePainter() : scene(0),maze(0)
{
}

MazePainter::MazePainter(QGraphicsScene *_scene, Maze *_maze, QColor _color) : scene(_scene),maze(_maze),color(_color)
{
    reloadMaze();
}

MazePainter::~MazePainter(){
}

void MazePainter::reloadMaze(){
    int w=maze->getWidth();
    int h=maze->getHeight();
    if(w*h==0){
        step=0;
    } else {
        step=graphics_width/(w>h?w:h);
    }
}

void MazePainter::draw() const{
    if(scene==0||maze==0){
        return;
    }
    int w=maze->getWidth();
    int h=maze->getHeight();

    QPen pen;
    QBrush brush(Qt::SolidPattern);
    pen.setColor(Qt::gray);

    //scene->clear();

    for(int i=0;i<=w;i++){
        scene->addLine(i*step,0,i*step,h*step,pen);
    }
    for(int i=0;i<=h;i++){
        scene->addLine(0,i*step,w*step,i*step,pen);
    }

    pen.setColor(color);
    pen.setWidth(2);
    for(int i=0;i<w;i++){
        for(int j=0;j<h;j++){
            if(i+1==w&&maze->isSetWall((h-j-1)*w+i,Maze::DirRight)){
                scene->addLine((i+1)*step,(j+1)*step,(i+1)*step,j*step,pen);
            }
            if(j+1==h&&maze->isSetWall((h-j-1)*w+i,Maze::DirBack)){
                scene->addLine(i*step,(j+1)*step,(i+1)*step,(j+1)*step,pen);
            }
            if(maze->isSetWall((h-j-1)*w+i,Maze::DirFront)){
                scene->addLine(i*step,j*step,(i+1)*step,j*step,pen);
            }
            if(maze->isSetWall((h-j-1)*w+i,Maze::DirLeft)){
                scene->addLine(i*step,(j+1)*step,i*step,j*step,pen);
            }
        }
    }
    pen.setStyle(Qt::NoPen);
    brush.setColor(Qt::yellow);
    scene->addRect(maze->getGoalX()*step+1,(h-maze->getGoalY()-1)*step+1,step-2,step-2,pen,brush);
}
