#include "mazesimulator.h"
#include "stopwatch.h"

#include <QFileDialog>
#include <QMessageBox>
#include <cmath>

using namespace MazeSolver;

MazeSimulator::MazeSimulator():state(0)
{
    scene=new QGraphicsScene();
    maze=new Maze();
    refMaze=new Maze();
	wallSource=new RefMazeWallSource(refMaze);
    agent=new Agent(maze,wallSource,&graph);
    mp=new MazePainter(scene,maze,Qt::red);
    rmp=new MazePainter(scene,refMaze,Qt::black);
    gp=new GraphPainter(scene,&graph);
    ap=new AgentPainter(scene,agent);
    t=new QTimer();
}

MazeSimulator::~MazeSimulator(){
    delete gp;
    delete mp;
    delete rmp;
    delete ap;
    delete agent;
    delete maze;
    delete refMaze;
    delete scene;
    delete t;
}

const QString MazeSimulator::loadMaze(){
    QString temp_filepath=QFileDialog::getOpenFileName(0,"Select Maze Data File","","Maze Data (*.dat)");
    if(temp_filepath.isNull()==false && temp_filepath.endsWith(".dat"))
    {
        scene->clear();
        filepath=temp_filepath;
        MazeLoader::load(filepath,*refMaze);
		MazeLoader::loadEmpty(refMaze->getWidth(), refMaze->getHeight(), refMaze->getGoalX(), refMaze->getGoalY(), *maze);
		graph.loadMaze(maze);
        rmp->reloadMaze();
        rmp->draw();
        w=refMaze->getWidth();
        h=refMaze->getHeight();
        agent->setIndex(w-1);
        agent->setDir(Maze::DirNorth);
        g_w=rmp->getWidth();
        g_h=rmp->getHeight();
        MazeLoader::loadEmpty(w,h,refMaze->getGoalX(),refMaze->getGoalY(),*maze);
        mp->reloadMaze();
        mp->draw();
        loaded=true;
    }
    return filepath;
}

void MazeSimulator::onClickMaze(const QPoint &point){
    if(!loaded) return;
    /*
    int mx=point.x()-(MazePainter::graphics_width-g_w)/2;
    int my=point.y()-(MazePainter::graphics_width-g_h)/2;

    int step=g_w/w;
    int x=mx/step;
    int y=my/step;

    if(x>w||y>h){
        return;
    }
    if(x==w){
        x=w-1;
    }
    if(y==h){
        y=h-1;
    }

    Direction dir;
    dir.half=0;
    if(hypot(mx-(2*x+1)*step/2,my-y*step)<step/3){
        // north
        dir=Maze::DirFront;
    } else if(hypot(mx-(x+1)*step,my-(2*y+1)*step/2)<step/3){
        // east
        dir=Maze::DirRight;
    } else if(hypot(mx-x*step,my-(2*y+1)*step/2)<step/3){
        // west
        dir=Maze::DirLeft;
    } else if(hypot(mx-(2*x+1)*step/2,my-(y+1)*step)<step/3){
        // south
        dir=Maze::DirBack;
    }
    if(dir.half){
        Coord c;
        c.x=x;
        c.y=h-y-1;
        maze->toggleWall(c,dir);
        mp->draw();
    }

    QMessageBox msg;
    msg.setText(QString().sprintf("%d,%d,%d",x,y,dir.half));
    //msg.exec();
    */
}

void MazeSimulator::runSimulation(){
    if(!loaded) return;
    graph.setWidth(w);
    graph.setHeight(h);
    graph.loadEmpty(w,h);
    agent->setIndex(w-1);
    agent->setDir(Maze::DirNorth);
	agent->reroute();
    goal=maze->getGoalNodeIndex();
    //graph.loadMaze(*maze);
    connect(t,SIGNAL(timeout()),this,SLOT(stepSimulationInvoker()));
    state=0;
    t->start(40);
}

void MazeSimulator::stepSimulationInvoker(){
    int status=stepSimulation();
    QMessageBox msg;
    if(status==1){
        state++;
        switch(state){
        case 1:
            goal=w-1;
            agent->reroute();
            break;
        case 2:
            agent->setIndex(w-1);
            agent->setDir(Maze::DirNorth);
            goal=maze->getGoalNodeIndex();
            agent->reroute();
            break;
        case 3:
            /*
            msg.setText("Finish!");
            msg.exec();
            t->stop();
            */
            goal=w-1;
            agent->reroute();
            state=1;
            break;
        default:
            msg.setText("Error! Abort.");
            msg.exec();
            t->stop();
            state=0;
        }
    } else if(status==-1){
        msg.setText("Cannot reach! Abort.");
        msg.exec();
        t->stop();
    }
}

int MazeSimulator::stepSimulation(){
    if(!loaded) return -1;
    int oldIndex=agent->getIndex();
    int index;
    if(state==2)
        index=agent->stepMaze(goal,false);
    else
        index=agent->stepMaze(goal,true);
    if(index<0){
        return -1;
    }

    scene->clear();
    //rmp->reloadMaze();
    //rmp->draw();
    mp->reloadMaze();
    mp->draw();
    gp->reloadGraph();
    //gp->drawGraph();

    if(index==oldIndex){
        // 壁にあたったので再度最短経路を導出
        index=agent->stepMaze(goal,true);
    }

    gp->drawRoute(agent->getRoute());
    ap->reloadAgent();
    ap->draw();

    if(index==goal){
        return 1;
    } else {
        return 0;
    }
}
