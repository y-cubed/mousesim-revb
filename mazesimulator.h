#ifndef MAZESIMULATOR_H
#define MAZESIMULATOR_H

#include <QGraphicsScene>
#include <QTimer>
#include "libmazesolver/maze.h"
#include "mazeloader.h"
#include "mazepainter.h"
#include "libmazesolver/graph.h"
#include "graphpainter.h"
#include "libmazesolver/agent.h"
#include "agentpainter.h"
#include "refmazewallsource.h"

#include <stack>
#include <map>

class MazeSimulator : public QObject
{
	Q_OBJECT
	public:
		MazeSimulator();
		~MazeSimulator();

		QGraphicsScene *getScene() const { return scene; }
		MazeSolver::Maze *getMaze() const { return maze; }

		const QString loadMaze();
		bool isLoaded() const{ return loaded; }

		void redrawMaze() const{ mp->draw(); }

		void runSimulation();

		int stepSimulation();

		void onClickMaze(const QPoint &point);

		private slots:
			void stepSimulationInvoker();

	private:
		QGraphicsScene *scene;
		MazeSolver::Maze *maze;
		MazeSolver::Maze *refMaze;
		MazeSolver::RefMazeWallSource *wallSource;
		MazePainter *mp;
		MazePainter *rmp;
		MazeSolver::Graph graph;
		GraphPainter *gp;
		MazeSolver::Agent *agent;
		AgentPainter *ap;
		QString filepath;
		QTimer *t;
		int w,h;
		int g_w,g_h;
		int goal;
		unsigned char state;
		bool loaded;
};

#endif // MAZESIMULATOR_H
