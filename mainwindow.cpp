#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>

#include "mazeloader.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(simulator.getScene());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::refresh()
{
    simulator.redrawMaze();
}

void MainWindow::on_exitButton_clicked()
{
    exit(0);
}

void MainWindow::on_loadButton_clicked()
{
    QString filepath=simulator.loadMaze();
    ui->filepathEdit->setText(filepath);
}

void MainWindow::on_action_Load_Maze_Data_triggered()
{
    QString filepath=simulator.loadMaze();
    ui->filepathEdit->setText(filepath);
}

void MainWindow::on_actionExit_triggered()
{
    exit(0);
}

void MainWindow::on_runButton_clicked()
{
    simulator.runSimulation();
}
