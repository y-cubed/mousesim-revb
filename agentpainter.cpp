#include "agentpainter.h"

#include <QGraphicsTextItem>

using namespace MazeSolver;

const int AgentPainter::graphics_width=320;

AgentPainter::AgentPainter():scene(0),agent(0),step(0){}
AgentPainter::AgentPainter(QGraphicsScene *_scene,Agent *_agent):scene(_scene),agent(_agent){
    reloadAgent();
}

void AgentPainter::reloadAgent(){
    int w=agent->getWidth();
    int h=agent->getHeight();
    if(w*h==0){
        step=0;
    } else {
        step=graphics_width/(w>h?w:h);
    }
}

void AgentPainter::draw(){
    int w=agent->getWidth();
    int h=agent->getHeight();
    int index=agent->getIndex();
    Direction dir=agent->getDir();
    int x=index%(2*w-1);
    int y=index/(2*w-1);
    QImage img;

    //QGraphicsTextItem *io=new QGraphicsTextItem();
    if(dir.half==0x1){
        img=QImage("assets/img/mouse_n.png");
        //io->setPlainText("▲");
    } else if(dir.half==0x2){
        img=QImage("assets/img/mouse_e.png");
        //io->setPlainText("▶");
    } else if(dir.half==0x4){
        img=QImage("assets/img/mouse_s.png");
        //io->setPlainText("▼");
    } else if(dir.half==0x8){
        img=QImage("assets/img/mouse_w.png");
        //io->setPlainText("◀");
    } else {

    }
    QGraphicsPixmapItem* mousepi = new QGraphicsPixmapItem(QPixmap::fromImage(img.scaled(step,step)));

    //io->setFont(QFont("monospace",step/2,-1,false));
    if(x<w-1){
        // EAST
        //io->setPos(step*(x+1)-step/2,step*(2*(h-y-1)+1)/2-step/2);
        mousepi->setPos(step*(x+1)-step/2,step*(2*(h-y-1)+1)/2-step/2);
    } else {
        // NORTH
        x-=w-1;
        //io->setPos(step*(2*x+1)/2-step/2,step*(h-y-1)-step/2);
        mousepi->setPos(step*(x+1)-step,step*(2*(h-y-1)+1)/2-step);
    }
    //scene->addItem(io);
    scene->addItem(mousepi);
}
