#include "myqgraphicsview.h"
#include <QMessageBox>
#include <QMouseEvent>
#include <QString>

MyQGraphicsView::MyQGraphicsView(QWidget *parent):QGraphicsView(parent){}

void MyQGraphicsView::mousePressEvent(QMouseEvent *event){
    QGraphicsView::mousePressEvent(event);
    emit pressed(event->pos());
}

void MyQGraphicsView::mouseMoveEvent(QMouseEvent *event){
    QGraphicsView::mouseMoveEvent(event);
}

void MyQGraphicsView::mouseReleaseEvent(QMouseEvent *event){
    QGraphicsView::mouseReleaseEvent(event);
}

void MyQGraphicsView::enterEvent(QEvent *event){
    QGraphicsView::enterEvent(event);
}

void MyQGraphicsView::wheelEvent(QWheelEvent *event){
    QGraphicsView::wheelEvent(event);
}

void MyQGraphicsView::leaveEvent(QEvent *event){
    QGraphicsView::leaveEvent(event);
}
