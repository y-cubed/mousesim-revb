#include "graphpainter.h"

#include "mazepainter.h"

using namespace MazeSolver;

const int GraphPainter::graphics_width=320;

GraphPainter::GraphPainter() : scene(0),g(0)
{
}

GraphPainter::GraphPainter(QGraphicsScene *_scene,Graph *_g) : scene(_scene),g(_g)
{
	reloadGraph();
}

GraphPainter::~GraphPainter(){
}

void GraphPainter::reloadGraph(){
	int w=g->getWidth();
	int h=g->getHeight();
	if(w*h==0){
		step=0;
	} else {
		step=graphics_width/(w>h?w:h);
	}
}

void GraphPainter::drawGraph() const{
	if(scene==0||g==0){
		return;
	}
	int w=g->getWidth();
	int h=g->getHeight();

	QPen pen;
	QBrush brush(Qt::SolidPattern);
	pen.setWidth(1);
	pen.setColor(Qt::blue);
	brush.setColor(QColor(0xff,0xb0,0x00,200));

	//scene->clear();
	for(int i=h-1;i>=0;i--){
		for(int j=0;j<w;j++){
			if(i<h-1){
				// NORTH
                //scene->addEllipse((2*j+1)*step/2-2,(h-i-1)*step-2,4,4,QPen(Qt::NoPen),brush);
				int index=i*(2*w-1)+j+w-1;
                std::vector<int16_t> edges=g->getEdges(index);
                for(auto it=edges.begin();it!=edges.end();it++){
                    int to=(*it);
                    if(to<0){
                        continue;
                    }
					int diff=to-index;
					if(diff==2*w-1){
						// N
						scene->addLine((2*j+1)*step/2,(h-i-1)*step,(2*j+1)*step/2,(h-i-2)*step,pen);
					}
				}
			}
			if(j<w-1){
				// EAST
                //scene->addEllipse((j+1)*step-2,(2*(h-i-1)+1)*step/2-2,4,4,QPen(Qt::NoPen),brush);
				int index=i*(2*w-1)+j;
                std::vector<int16_t> edges=g->getEdges(index);
                for(auto it=edges.begin();it!=edges.end();it++){
                    int to=(*it);
                    if(to<0){
                        continue;
                    }
					int diff=to-index;
					if(diff==w){
						// NE
						scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+3)*step/2,(h-i-1)*step,pen);
					} else if(diff==1){
						// E
						scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(j+2)*step,(2*(h-i-1)+1)*step/2,pen);
					} else if(diff==1-w){
						// SE
						scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+3)*step/2,(h-i)*step,pen);
					} else if(diff==-w){
						// SW
						scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+1)*step/2,(h-i)*step,pen);
					} else if(diff==w-1){
						// NW
						scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+1)*step/2,(h-i-1)*step,pen);
					}
				}
			}
		}
	}
}

void GraphPainter::drawRoute(const Route &route) const {
	int w=g->getWidth();
	int h=g->getHeight();
	QPen pen;
	pen.setWidth(2);
	pen.setColor(Qt::red);
	int lastindex=route.front();
	for(auto it=route.begin();it!=route.end();it++){
		Node node=g->getNode(lastindex);
		Coord c=node.getCoord(w);
		int j=c.x;
		int i=c.y;
		int diff=(*it)-lastindex;
		if(diff==2*w-1){
			// N
			scene->addLine((2*j+1)*step/2,(h-i-1)*step,(2*j+1)*step/2,(h-i-2)*step,pen);
		} else if(diff==w&&c.dir.half==1){
			// N-NE
			scene->addLine((j+1)*step,(2*(h-i-1)-1)*step/2,(2*j+1)*step/2,(h-i-1)*step,pen);
		} else if(diff==w&&c.dir.half==2){
			// E-NE
			scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+3)*step/2,(h-i-1)*step,pen);
		} else if(diff==1){
			// E
			scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(j+2)*step,(2*(h-i-1)+1)*step/2,pen);
		} else if(diff==1-w&&c.dir.half==1){
			// N-SE
			scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+1)*step/2,(h-i-1)*step,pen);
		} else if(diff==1-w&&c.dir.half==2){
			// E-SE
			scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+3)*step/2,(h-i)*step,pen);
		} else if(diff==-2*w+1){
			// S
			scene->addLine((2*j+1)*step/2,(h-i-1)*step,(2*j+1)*step/2,(h-i)*step,pen);
		} else if(diff==-w&&c.dir.half==1){
			// N-SW
			scene->addLine(j*step,(2*(h-i-1)+1)*step/2,(2*j+1)*step/2,(h-i-1)*step,pen);
		} else if(diff==-w&&c.dir.half==2){
			// E-SW
			scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+1)*step/2,(h-i)*step,pen);
		} else if(diff==-1){
			// W
			scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,j*step,(2*(h-i-1)+1)*step/2,pen);
		} else if(diff==w-1&&c.dir.half==1){
			// N-NW
			scene->addLine(j*step,(2*(h-i-1)-1)*step/2,(2*j+1)*step/2,(h-i-1)*step,pen);
		} else if(diff==w-1&&c.dir.half==2){
			// E-NW
			scene->addLine((j+1)*step,(2*(h-i-1)+1)*step/2,(2*j+1)*step/2,(h-i-1)*step,pen);
		}
		lastindex=(*it);
	}
}
