#include <iostream>
#include "stopwatch.h"

void StopWatch::start(){
#ifdef _WIN32
    begin=GetTickCount();
#else
	begin=get_dtime();
#endif
}

void StopWatch::stop(){
#ifdef _WIN32
    end=GetTickCount();
#else
	end=get_dtime();
#endif
}

void StopWatch::show() const{
#ifdef _WIN32
    std::cout<<"<StopWatch>: "<<(double)(end-begin)/1000.0<<"ms"<<std::endl;
#else
    std::cout<<"<StopWatch>: "<<(end-begin)*1000.0<<"ms"<<std::endl;
#endif
}

double StopWatch::getDuration() const{
#ifdef _WIN32
    return (double)(end-begin)/1000.0;
#else
    return end-begin;
#endif
}

#ifndef _WIN32
double StopWatch::get_dtime() const{
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((double)(tv.tv_sec) + (double)(tv.tv_usec) * 0.001 * 0.001);
}
#endif
