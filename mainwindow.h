#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QFileDialog>
#include <QGraphicsScene>
#include "libmazesolver/maze.h"
#include "mazepainter.h"
#include "mazesimulator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void refresh();

private:
    void refresh_maze();
    void load_maze();
    
private slots:
    void on_exitButton_clicked();
    void on_loadButton_clicked();

    void on_action_Load_Maze_Data_triggered();

    void on_actionExit_triggered();

    void on_runButton_clicked();

private:
    Ui::MainWindow *ui;
    MazeSimulator simulator;
};

#endif // MAINWINDOW_H
