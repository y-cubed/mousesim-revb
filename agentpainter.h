#ifndef AGENTPAINTER_H
#define AGENTPAINTER_H

#include <QGraphicsScene>
#include "libmazesolver/agent.h"

class AgentPainter
{
public:
    static const int graphics_width;
public:
    AgentPainter();
    AgentPainter(QGraphicsScene *_scene,MazeSolver::Agent *_agent);
    ~AgentPainter(){}
    void draw();
    void reloadAgent();

    qreal getWidth() const { return step*agent->getWidth(); }
    qreal getHeight() const { return step*agent->getHeight(); }
private:
    QGraphicsScene *scene;
	MazeSolver::Agent *agent;
    int step;
};

#endif // AGENTPAINTER_H
